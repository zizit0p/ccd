<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use src\controler\SessionController;
use src\controler\ControllerListe;
use src\view\Vue;
use src\view\VueListe;
use src\view\VueApropos;
use src\view\VuePanier;
use src\controler\ControllerCagnotte;

$app = new \Slim\Slim();

$db = new DB();
$tab = parse_ini_file('db.config.ini');
$db->addConnection($tab);
$db->setAsGlobal();
$db->bootEloquent();


$app->get('/prestations', function (){
    echo ControllerListe::all();
})->name("/prestations");

$app->post('/prestations', function(){
    if(isset($_POST['filter']) && isset($_POST['cat']) && isset($_POST['pick']))
        echo 'Compte créé';
});

$app->get('/', function(){
	echo ControllerListe::acceuil();
})->name("/");

$app->get('/activites', function(){
	$tab = array();
	$tab['cat'] = 1;
	echo ControllerListe::all($tab);
})->name("/activites");

$app->get('/attentions', function(){
	$tab = array();
	$tab['cat'] = 2;
	echo ControllerListe::all($tab);
})->name("/attentions");

$app->get('/restaurations', function(){
	$tab = array();
	$tab['cat'] = 3;
	echo ControllerListe::all($tab);
})->name("/restaurations");

$app->get('/hebergements', function(){
	$tab = array();
	$tab['cat'] = 4;
	echo ControllerListe::all($tab);
})->name("/hebergements");

$app->get('/apropos', function(){
	echo ControllerListe::propos();
})->name("/apropos");

$app->get('/cagnotte', function(){
    echo ControllerCagnotte::cagnotte();
})->name("/cagnotte");

$app->get('/savecontr', function(){
    echo ControllerCagnotte::aiderCagnotte();
})->name("/savecontr");

$app->get('/panier', function(){
	$v = new VuePanier();
	echo $v->afficherPanier();
})->name("/panier");

$app->post('/panier', function(){
	echo '<p>Pardon..</p>';
    echo '<audio src="web/assets/musique/sad.mp3" autoplay loop></audio>';
})->name("/panier");

/*
$app->get('/login', function (){
    $vueLogin = new \src\views\VueLogin();
    $vueLogin->genererContent();
    echo $vueLogin->rendre();

});




$app->get('/subscribe', function (){
    $vueInscription = new \src\views\VueInscription();
    $vueInscription->genererContent();
    echo $vueInscription->rendre();
});

$app->post('/subscribe', function (){
    if(isset($_POST['login']) && isset($_POST['password']) && isset($_POST['verify'])
        && isset($_POST['nom'])&& isset($_POST['prenom'])&& isset($_POST['adresse'])
        && isset($_POST['email'])&& isset($_POST['numtel'])) {
        echo 'Compte créé';
    }
    SessionController::createUser($_POST);
});

$app->post('/login', function (){
    if(isset($_POST['login']) && isset($_POST['password'])) {
        SessionController::authenticate($_POST['login'],$_POST['password']);
    }
});

$app->get('/compte', function(){
    echo 'mon compte';
} );

$app->post('/compte', function(){
    echo 'mon compte';
} );

$app->get('/deconnexion', function(){
    SessionController::closeSession();
    $app = \Slim\Slim::getInstance();
    $url = $app->urlFor('accueil');
    $app->redirect($url);
});

$app->get('/', function(){
    echo 'accueil';
} )->name('accueil');
*/
try {
    $app->run();
}catch(Exception $e){
    $app->redirect('accueil');
}

