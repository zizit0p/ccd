<?php

namespace src\controler;
use src\models\Prestation;
use src\models\Pochette;
use src\models\PrestaDeLaPochette;
use src\view\VueApropos;
use src\view\VueListe;
use src\view\VueAcceuil;
use src\models\Cagnotte;

/**
 * Created by Brackets.
 * User: Max
 * Date: 11/02/2016
 * Time: 11:25
 */

class ControllerListe{
    
    /**
    * Fonction qui récupère toutes les prestations correspondantes aux filtres
    *       puis les passe en paramètres de la fonction render
    * @param $tab:array
    *       'filter' :  true pour afficher en croissant par prix
    *                   false pour afficher en décroissant par prix
    *                   null pour afficher normalement
    *       'nom' : true pour trier par nom en croissant
    *               false pour trier par nom en décroissant
    *               null pour afficher normalement
    *       'cat' : pour afficher uniquement cette catégorie
    *               null pour tout afficher
    */
    public static function all ($tab=null){
        $list = array();
        //$list = Prestation::all()->take(Nombre d'items a affichés)
        if(isset($tab['cat'])){
            //$list = Prestation::all()->where('type','=',$tab['cat']);
            $list = Prestation::where('type', '=', $tab['cat'])->get();

		}
        else
            $list = Prestation::all();
        
        if(isset($tab['tri'])){
			if ($tab['tri']=='nom'){
				$list->orderBy('nom');
			}else{
				if($tab['tri']=='prix'){
					$list->orderBy('prix');
				}
				else{
					$list->orderBy('prix','descr');
				}
			}
		}
        $vue = new VueListe($list);
        return $vue->render(VueListe::AFF_PRESTATION);

    }
    public static function propos()
    {
        $view = new VueApropos();
        return $view->render();
    }

    public static function acceuil(){
            $view = new VueAcceuil();
            return $view->render();
        }

    public static function ajoutCagnotte(){
        $c=new Cagnotte();
        $c->nom=filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
        $c->montant=filter_var($_POST['montant'], FILTER_SANITIZE_STRING);
        $c->idPoch=$_SESSION['idPoch'];
        $c->save();
    }

	public static function addpan(){
		$p = new Pochette();
		$p->idPoch = Pochette::getPdo()->lastInsertId();
		$p->montanttotal = 0;
		$p->save();
		$pp = new PrestaDeLaPochette();
		$v = new VueListe();
		$product;
		if(sizeof($_POST)>0){
            if(!isset($_SESSION['panier'])) {
                $_SESSION['panier'] = array();
            }
            foreach($_POST as $id => $quantite) {
                $product = Prestation::find($id);
                if (isset($product) && !is_null($quantite) && intval($quantite) > 0) {
                    $_SESSION['panier'][$product->nom] = array(intval($quantite), $product->prix); // stocke le produit dans la session
                }

            }
            $app = Slim::getInstance();
            $app->redirectTo('/panier');
        }
		$pp->idPoch = $p->idPoch;
		$pp->idPresta = $product->id;
		$p->montanttotal += $product->prix;
		$p->save();
		$pp->save();
	}
	
}
