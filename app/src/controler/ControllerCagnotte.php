<?php

namespace src\controler;
use src\view\VueCagnotte;

class ControllerCagnotte {
	public static function aiderCagnotte(){
		$n = $_POST['nom'];
		$m = $_POST['montant'];
		$i = uniqid();
		$c = new Cagnotte();
		$c->id = $i;
		$c->nomparticipant = $n;
		$c->montant = $m;
		$c->save();
	}

    public static function cagnotte(){
        $v = new VueCagnotte();
        return $v->render();
    }
}
