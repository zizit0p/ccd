<?php

namespace src\controler;
use src\models\Client;

/**
 * Created by PhpStorm.
 * User: Max
 * Date: 13/01/2016
 * Time: 13:03
 */

class SessionController{

    public static function createUser ($tab){
        // vérifier la conformité de $password avec la police
        // si ok : hacher $password
        // créer et enregistrer l'utilisateur
        /*
        $policy = new \PasswordPolicy\Policy;
        $policy->contains('lowercase', $policy->atLeast(2));
        $policy->length( 6 ) ;
        $js = $policy->toJavaScript() ; echo "var policy = $js" ;
        $result = $policy->check( $password ) ;*/

        var_dump($tab);
        $user = new Client();
        $user->idClient = filter_var($tab['login'], FILTER_SANITIZE_STRING);
        $user->nom = filter_var($tab['nom'], FILTER_SANITIZE_STRING);
        $user->prenom = filter_var($tab['prenom'],FILTER_SANITIZE_STRING);
        $user->alea = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        $user->hash = password_hash($tab['password'], PASSWORD_BCRYPT, array('salt' => $user->alea));
        $user->niveauNat = filter_var($tab['niveauNat'],FILTER_SANITIZE_STRING);
        $user->adresse = filter_var($tab['adresse'],FILTER_SANITIZE_STRING);
        $user->email = filter_var($tab['email'],FILTER_SANITIZE_STRING);
        $user->numtel = filter_var($tab['numtel'],FILTER_SANITIZE_STRING);
        $user->save();

        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor('accueil');
        $app->redirect($url);
    }

    public static function authenticate ( $username, $password ) {
        // charger utilisateur $user
        // vérifier $user->hash == hash($password)
        // charger profil ($user->id)
        $user = \src\models\Client::where('idClient', '=', $username)->first();
        if(password_verify($password,$user->hash)) {
            session_start();
            SessionController::loadProfile($user);
        }
    }

    private static function loadProfile( $user ) {
        // charger l'utilisateur et ses droits
        // détruire la variable de session
        // créer variable de session = profil chargé

        $_SESSION['idClient']= filter_var($user->idClient, FILTER_SANITIZE_STRING);
        $_SESSION['nom']= filter_var($user->nom, FILTER_SANITIZE_STRING);
        $_SESSION['prenom']= filter_var($user->prenom,FILTER_SANITIZE_STRING) ;
        $_SESSION['niveauNat']= filter_var($user->niveauNat,FILTER_SANITIZE_STRING);
        $_SESSION['adresse']= filter_var($user->adresse,FILTER_SANITIZE_STRING);
        $_SESSION['email']= filter_var($user->email,FILTER_SANITIZE_STRING);
        $_SESSION['numtel']= filter_var($user->numtel,FILTER_SANITIZE_STRING);
        $_SESSION['famille'] = 'aucune';
        $_SESSION['commande']= array();

        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor('accueil');
        $app->redirect($url);
    }

    public static function modifierInfos(){
        session_start();
        $user = Client::find($_SESSION['idClient'])->first();
        $user = $_POST['nom'];
        $user = $_POST['prenom'];
        $user = $_POST['adresse'];
        $user = $_POST['email'];
        $user = $_POST['numtel'];
        $user = $_POST['famille'];
        $user->save();
    }

    public static function closeSession(){
        session_start();
        session_destroy();
    }
    /*public static function checkAccessRights ( $required ) {
        // si Authentication::$profil['level] < $required
        throw new AuthException ;
    }*/

}