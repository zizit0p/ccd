<?php
namespace src\view;
use src\models\Prestation;
use src\models\Type;
use Slim\Slim;

class VuePanier extends Vue{
	
	public function __construct($tab=null){
		parent::__construct($tab);
	}
	
	public function render($sel){
		$content=$this->afficherpanier();
		return $this->squelette($content);
	}
				
	public function afficherpanier(){
		$uri = Slim::getInstance()->request()->getRootUri();
		$prix_total = 0;
		$html = '
            <div class="panel panel-default">
                <div class="page-header">
                    <h3 class="text-center">Panier</h3>
                </div>
                <div class="panel-body">
            ';
		if(isset($_SESSION['panier']) && sizeof($_SESSION['panier']) > 0) {
			$html .='
                        <h4 class="text-center text-info">Liste des articles de votre panier</h4>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Quantité</th>
                                <th>Prix unitaire</th>
                                <th>Prix total</th>
                            </tr>
                            </thead>
                            <tbody>
                            ';
		
			foreach($_SESSION['panier'] as $product_nom => $array){
				$html .= '
                    <tr>
                        <td>' . $product_nom . '</td>
                        <td>' . $array[0] . '</td>
                        <td>' . $array[1] . '</td>
                        ';
				$val = 0;
				if(!is_null($array[0]) &&  !is_null($array[1])){
					$val = $array[0] * $array[1];
				}
				$html .= '
                        <td>'.$val.'</td>
                    </tr>
                ';
				$prix_total = $prix_total + $val;
			}
			$_SESSION['panier_prix_total'] = $prix_total; // stocke et met à jour le prix total du panier
			$html .= '
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>'.$prix_total.'</td>
                </tr>
            ';
			$html .= '
                    </tbody>
                </table>
                <div class="pull-right">
                    <a href="'.$uri.'/panier/facture" class="btn btn-primary">Payer plus tard</a>
                    <a href="'.$uri.'/payement_en_ligne" class="btn btn-primary">Payer en ligne</a>
                </div>
            ';
		}
		else{
			$html .= '
                <div class="text-center">
                    <h4 class="text-info">Votre panier est vide !</h4>
                    <a href="'.$uri.'/prestations" class="btn btn-primary">Accéder au prestations</a>
                </div>
            ';
		
		}
		$html .= '
                    </div>
                </div>
                ';
		
		return $html;
			
	}
	
}
