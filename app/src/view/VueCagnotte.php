<?php
namespace src\view;
use src\models\Prestation;
use src\models\Type;
    
class VueCagnotte extends Vue{
	public function __construct($tab=null){
		parent::__construct($tab);
	}	
	
	public function render(){
		$content=$this->afficherCagnotte();
		return $this->squelette($content);
	}
	
	public function afficherCagnotte(){
		$uri = $this->root_uri."/ajoutParticipant";
		$html = <<<END
        <div align="center">
		<form id="participant" methode="post" action="$uri/savecontr">
        <input type="text" name="nom" placeholder="Inserer votre nom"><br />
		<input type="number" name="montant" placeholder="Inserer votre montant"><br />
		<button type="submit">Valider</button>
		</form>
        </div>
END;
		return $html;
	}
}
