<?php
namespace src\view;
use Slim\Slim;

class VueAcceuil extends Vue {

    protected $root_uri;
    public function render(){
        return $this->squelette($this->afficherDiapo());
    }

    public function afficherDiapo(){
        $uri = $this->root_uri;
        $this->root_uri=Slim::getInstance()->request()->getRootUri();
    return '
    <audio src="web/assets/musique/accmusic.mp3" autoplay loop></audio>
    <div align="center">
        <div  class="slideshow">
            <ul>
                <li><img src="web/assets/img/animateur.jpg" alt="" width="700" height="400" /></li>
                <li><img src="web/assets/img/cocktail.jpg" alt="" width="700" height="400" /></li>
                <li><img src="web/assets/img/place.jpg" alt="" width="700" height="400" /></li>
                <li><img src="web/assets/img/opera.jpg" alt="" width="700" height="400" /></li>
            </ul>
        </div>

    </div>
    <section>

            <article>
                <div class="jumbotron">
                    <h1>Nos services :</h1>
                    <p>Envie de surprendre vos proches pour une occasion spéciale ? </p>
                    <p>La pochette surprise est faite pour vous ! </p>
                    <p>Choisissez au minimum trois prestations et créer vous même une pochette surprise ! </p>
                    <p>Parfait pour des occasions tel un anniversaire, cette pochette surprise contribuera également au commerce local.</p>
                    <p>Partagez ce cadeau avec vos amis afin de remplir les objectifs de la cagnottes.</p>

                    <div align="center">
                        <a class="twitter-timeline" width="280" height="300" href="https://twitter.com/PochSurprise54" data-widget-id="697743091172777985">Tweets by @PochSurprise54</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                        </script>
                    </div>
                </div>

            </article>

    </section>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

    <script type="text/javascript">
   $(function(){
      setInterval(function(){
         $(".slideshow ul").animate({marginLeft:-700},800,function(){
            $(this).css({marginLeft:0}).find("li:last").after($(this).find("li:first"));
         })
      }, 3500);
   });
</script>
    ';

    }
}