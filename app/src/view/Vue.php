<?php

namespace src\view;
require_once 'vendor/autoload.php';
use src\models\Prestation;
use src\models\Type;
use Slim\Slim;

class Vue {

    protected $obj;
	protected $root_uri;

	public function __construct($tab=null){
		
		$this->obj=$tab;
		
		$this->root_uri=Slim::getInstance()->request()->getRootUri();
	}

    public function squelette($content){
    	$uri = $this->root_uri;
       $html = <<<END
           <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8" />
                    <link rel="stylesheet" href="web/assets/stylesheet/app.css">
                    <title>Ma pochette cadeau</title>
                </head>

                <body>
                    <header>
                    </header>

    <style>
    #slide{
        -webkit-transition: margin-left 0.7s ease-in-out;
        -moz-transition: margin-left 0.7s ease-in-out;
        -ms-transition: margin-left 0.7s ease-in-out;
        -o-transition: margin-left 0.7s ease-in-out;
        transition: margin-left 0.7s ease-in-out;
    }
    #slide{
        margin-left:-30px;
        width:52px;
        position:absolute;
        top: 250px;
        height: auto;
        border-radius: 2px;
        background-color: bisque;
    }
    img {
        display:block;
    }
    #slide:hover{
        margin-left:-10px;
    }
    #slide .it{
        display:none;
    }
    #slide p {
        visibilty:hidden;
        display:inline;
        opacity: 0;
        transition: opacity 0.5s linear;
        transition-delay: 0.5s;
    }
    #item{
        display:inline;
    }
    #item:onclick {
        transition: width 1s ease-in-out;
    }
    </style>
    <div id="slide">
        <form id="panier" method="post" action="panier">
            <input type="submit" name="commit" value="Panier"></input>
        </form>
    </div>

    <script type="text/javascript">
    function toggle_visibility(id){
        var form = document.getElementById('panier');
        var item = document.getElementById(id).lastChild;
        var inp = document.createElement("input");
        var p = document.createElement("p");
        inp.setAttribute("type","text");
        inp.setAttribute("name","prests[]");
        inp.setAttribute("value",id);
        inp.setAttribute("class","it");
        form.appendChild(inp);
        if(form.getElementsByTagName('input').length >= 3)
            form.disable = true;
        else
            form.disable = false;
    }
    </script>



                    <nav class="navbar navbar-inverse">

              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="$uri">Pochette Surprise 54</a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                  <ul class="nav navbar-nav">
                    <li class="active"><a href="$uri">Accueil <span class="sr-only">(current)</span></a></li>
                    <li><a href="$uri/prestations">Prestations</a></li>
                    <li class="dropdown" id="dym" onClick="navCateg.navOpenClose()">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Catégories <span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="$uri/activites">Activités</a></li>
                        <li><a href="$uri/attentions">Attentions</a></li>
                        <li><a href="$uri/restaurations">Restauration</a></li>
                        <li><a href="$uri/hebergements">Hébergements</a></li>
                      </ul>
                    </li>
                  </ul>

                  <ul class="nav navbar-nav navbar-right">
                  	<li><a href="$uri/panier">Panier</a></li>
                    <li><a href="$uri/connexion">Connexion</a></li>
                    <li><a href="$uri/apropos">A propos</a></li>
                  </ul>

                </div>
              </div>

            </nav>

                    <section>
                        $content
                    </section>

                    <script>
                    var navCateg = {
                        ouvert : false,
                        navOpenClose : function(){
                            if(navCateg.ouvert === false){
                                navCateg.ouvert = true;
                                document.getElementById("dym").className = "dropdown open";
                            }else{
                                navCateg.ouvert = false;
                                document.getElementById("dym").className = "dropdown";
                            }
                        }
                    }


                    </script>
                    
                </body>
                <style>
                    #slide {
                        position : absolute
                    }
                </style>
               
            </html>
END;
        return $html;
    }


        public function panier(){
        $uri = Slim::getInstance()->request()->getRootUri();
        $prix_total = 0;
        $html = '
            <div class="panel panel-default">
                <div class="page-header">
                    <h3 class="text-center">Panier</h3>
                </div>
                <div class="panel-body">
            ';
        if(isset($_SESSION['panier']) && sizeof($_SESSION['panier']) > 0) {
            $html .='
                        <h4 class="text-center text-info">Liste des articles de votre panier</h4>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Quantité</th>
                                <th>Prix unitaire</th>
                                <th>Prix total</th>
                            </tr>
                            </thead>
                            <tbody>
                            ';

            foreach($_SESSION['panier'] as $product_nom => $array){
                $html .= '
                    <tr>
                        <td>' . $product_nom . '</td>
                        <td>' . $array[0] . '</td>
                        <td>' . $array[1] . '</td>
                        ';
                $val = 0;
                if(!is_null($array[0]) &&  !is_null($array[1])){
                    $val = $array[0] * $array[1];
                }
                $html .= '
                        <td>'.$val.'</td>
                    </tr>
                ';
                $prix_total = $prix_total + $val;
            }
            $_SESSION['panier_prix_total'] = $prix_total; // stocke et met à jour le prix total du panier
            $html .= '
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>'.$prix_total.'</td>
                </tr>
            ';
            $html .= '
                    </tbody>
                </table>
                <div class="pull-right">
                    <a href="'.$uri.'/panier/facture" class="btn btn-primary">Payer plus tard</a>
                    <a href="'.$uri.'/payement_en_ligne" class="btn btn-primary">Payer en ligne</a>
                </div>
            ';
        }
        else{
            $html .= '
                <div class="text-center">
                    <h4 class="text-info">Votre panier est vide !</h4>
                    <a href="'.$uri.'/boutique" class="btn btn-primary">Accéder à la boutique</a>
                </div>
            ';

        }
        $html .= '
                    </div>
                </div>
                ';

        return $html;
    }
}