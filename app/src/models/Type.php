<?php

namespace src\models;
use Illuminate\Database\Eloquent\Model;

class Type extends Model {
	protected $table='type';
	protected $primary_key='id';
	public $timestamps=false;
	
	public function prestations(){
		return $this->hasMany('\src\models\Prestation', 'id');
	}
}