<?php

namespace src\models;
use Illuminate\Database\Eloquent\Model;

class PrestaDeLaPochette extends Model {
	protected $table='prestadelapochette';
	protected $primary_key='id, idPoch';
	public $timestamps=false;
}