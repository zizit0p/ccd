<?php

namespace src\models;
use Illuminate\Database\Eloquent\Model;

class Cagnotte extends Model {
	protected $table='cagnotte';
	protected $primary_key='id';
	public $timestamps=false;
}