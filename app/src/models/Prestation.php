<?php

namespace src\models;
use Illuminate\Database\Eloquent\Model;

class Prestation extends Model {
	protected $table='prestation';
	protected $primary_key='id';
	public $timestamps=false;
	
	public function type(){
		return $this->belongsTo('\src\models\Type','id');
	}
}