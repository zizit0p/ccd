<?php

namespace src\models;
use Illuminate\Database\Eloquent\Model;

class Pochette extends Model {
	protected $table='pochette';
	protected $primary_key='idPoch';
	public $timestamps=false;
}