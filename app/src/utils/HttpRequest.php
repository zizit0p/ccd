<?php

namespace limaga_app\utils;

class HttpRequest{
	
	protected $_v=array();
	
	public function __construct(){
		$this->method= $_SERVER['REQUEST_METHOD'];
		$this->script_name= $_SERVER['SCRIPT_NAME'];
		if(isset($_SERVER['PATH_INFO'])){
			$this->path_info= $_SERVER['PATH_INFO'];
		}else{
			$this->path_info= null;
		}
		$this->query= $_SERVER['QUERY_STRING'];
		$this->get  = $_GET;
		$this->post = $_POST;
	}
	
	public function __get($att){
		if(array_key_exists($att, $this->_v)){
			return $this->_v[$att];
		}
		$err = get_called_class() . ": unknown attribute $att (getAttr)";
	}
	
	public function __set($att, $val){
		$this->_v[$att] = $val;
	}
	
	public function rootUri(){
		$res = dirname($this->script_name);
		return $res;
	}
}