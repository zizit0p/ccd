SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE TABLE IF NOT EXISTS `pochette` (
	`idPoch` int(13) NOT NULL,
	`montantTotal` int (11) NOT NULL,
	PRIMARY KEY (`idPoch`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `cagnotte`(
	`nomparticipant` text NOT NULL,
	`idPoch` int(13) NOT NULL,
	`montant` int (11),
	KEY `pochette` (`idPoch`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `prestadelapochette` (
	`idPoch` int (13) NOT NULL,
	`id` int (11) NOT NULL, 
	KEY `pochette` (`idPoch`), 
	KEY `cagnotte` (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 ;