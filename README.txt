--- BEWARE TREPASSER WILL BE ... BANNED ? ---

Membre du groupe :

PIERRE Alexandre
RUHLMANN Benjamin 
DOLET Maxime SI2
WIRTZ Thomas
PINHEIRO Lucas

Fonctionnalité principales :

    Afficher la liste des prestations
    Création de la pochette
    Ajout de prestations dans la pochette
    Cagnotte
    Click dans la liste
    Afficher le détail d’une prestation
    Devoilement de la pochette
    ULR à offrir
    Participer à la cagnotte 
    Filtrage par type 
    Tri par prix

Fonctionnalités secondaires :

    Option sécrète
    Ajout de prestations
    Notation des pretations
    Notes moyennes
    Ajout de critères
    Filtrage par critère
    Génération automatique de pochette mystère
    Inscriptions des prestataires partenaires
    Gestion des prestations
    Gestion des commandes